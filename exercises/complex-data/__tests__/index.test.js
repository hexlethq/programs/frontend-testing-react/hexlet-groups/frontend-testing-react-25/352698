const faker = require('faker');

// BEGIN
test ('objects have the same types and structure', () => {
    const transaction = faker.helpers.createTransaction();
    expect(transaction).toStrictEqual({
        amount: expect.any(String),
        date: expect.any(Date),
        business: expect.any(String),
        name: expect.any(String),
        type: expect.any(String),
        account: expect.any(String),
    });
});

test ('new data is generated each time', () => {
    const transactionOne = faker.helpers.createTransaction();
    const transactionTwo = faker.helpers.createTransaction();
    expect(transactionOne).not.toBe(transactionTwo);
});
// END
