const nock = require('nock');
const axios = require('axios');
const { get, post } = require('../src/index.js');

nock.disableNetConnect();
axios.defaults.adapter = require('axios/lib/adapters/http');
// BEGIN
const user = {
    firstname: 'Fedor',
  lastname: 'Sumkin',
  age: 33
};

describe('http-метод post', () => {
    test('создает данные со статусом 201', async () => {

        const scope = nock('https://example.com')
          .post('/users')
          .reply(201, user);
    
        const { data, status } = await post('https://example.com/users');
    
        expect(data).toEqual(user);
        expect(status).toBe(201);
      });
});

describe('http-метод get', () => {
    test('возвращает данные со статусом 200', async () => {
        const usersStub = Array.from({ length: 3 }, () => user);

        const scope = nock('https://example.com')
          .get('/users')
          .reply(200, usersStub);
    
        const { data, status } = await get('https://example.com/users');
    
        expect(data).toEqual(usersStub);
        expect(status).toBe(200);
      });
});

// END
