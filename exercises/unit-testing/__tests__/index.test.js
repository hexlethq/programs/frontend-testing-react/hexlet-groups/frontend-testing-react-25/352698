test('main', () => {
  const src = { k: 'v', b: 'b' };
  const target = { k: 'v2', a: 'a' };
  const result = Object.assign(target, src);

  expect(result).toEqual({"k": "v", "a": "a", "b": "b"});
  expect(result).toEqual(target);
  expect(result).not.toBe(src);

   
  expect.hasAssertions();
   
  // BEGIN
  expect(result).toBe(target);
});
   
  test('cloning an object to an empty object', () => {
    const obj = {a: 1};
    const result = Object.assign({}, obj);

    expect(result).toEqual(obj);
  });
   
  test('the enumerated property is copied to the new object', () => {
    const obj1 = Object.create({a: 1}, {
      c: {
        value: 3,
        enumerable: true
      },
      d: {
        value: 4,
        enumerable: false
      },
    });
    const obj2 = ({b: 2});
    const result = Object.assign({}, obj1, obj2);
   
    expect(result).toEqual({'c': 3, 'b': 2});
  });

  test('error return for empty arguments', () => {
  const result = () => Object.assign();
    expect(result).toThrow();
  });

  // END