const { array } = require('fast-check');
const fc = require('fast-check');

const sortAsc = (data) => data.slice().sort((a, b) => a - b);
const sortDesc = (data) => data.slice().sort((a, b) => b - a);

// BEGIN
test ('Invariance', () => {
    fc.assert (
        fc.property(
            fc.int16Array(),
            (arr) => {
                const sorted = sortAsc(arr);
                expect(sortAsc(arr).length).toBe(arr.length);
                expect(sortAsc(arr)).toBeSorted({ descending: false });
            })
        );
});

test ('Reverse array', () => {
    fc.assert (
        fc.property(
            fc.int16Array(),
            (arr) => {
                const array = arr.slice().reverse();

                expect(sortAsc(arr)).toEqual(sortAsc(array));
                expect(sortAsc(arr).reverse()).not.toBe(sortAsc(array));
                expect(sortAsc(arr)).toBeSorted({ descending: false });
            })
        );
});

test ('Re-sorting the array', () => {
    fc.assert (
        fc.property(
            fc.int16Array(),
            (arr) => {
                const array = sortAsc(arr);
                const result = sortAsc(array);
                
                expect(array).toEqual(result);
            })
        );
});

test ('Sort array', () => {
    fc.assert (
        fc.property(
            fc.int16Array(),
            (arr) => {
                expect(sortAsc(arr).reverse()).toEqual(sortDesc(arr));
            }
        )
    )
});
// END
