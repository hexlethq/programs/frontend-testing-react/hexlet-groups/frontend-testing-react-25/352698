const puppeteer = require('puppeteer');
const getApp = require('../server/index.js');

const port = 5001;
const appUrl = `http://localhost:${port}`;
const appArticlesUrl = `http://localhost:${port}/articles`;

let browser;
let page;

const app = getApp();

describe('simple blog works', () => {
  beforeAll(async () => {
    await app.listen(port, '0.0.0.0');
    browser = await puppeteer.launch({
      args: ['--no-sandbox', '--disable-gpu'],
      headless: true,
      // slowMo: 250
    });
    page = await browser.newPage();
    await page.setViewport({
      width: 1280,
      height: 720,
    });
  });

  // // BEGIN
  test('Главная страница приложения открывается', async () => {
    await page.goto(appUrl);
    const text = await page.$eval('#title', (e) => e.innerText);
    expect(text).toContain('Welcome to a Simple blog!');
  });

  test('Мы можем перейти на страницу со всеми статьями и увидеть там их список', async () => {
    await page.goto(`${appUrl}/articles`);
    const articles = await page.$$('#articles tr');
    expect(articles.length).toBeGreaterThan(0);
  });

  test('Можем нажать на кнопку создать статью и увидеть форму', async() => {
    await page.goto(appArticlesUrl);
    await page.click('[href="/articles/new"]');
    const forms =  await page.$('form');
    expect(forms).toBeDefined();
  });

  test('Может заполнить форму и создать новую статью. Нас перенаправляет на страницу со всеми статьями. В списке есть новая статья.', async() => {
    await page.goto(`${appArticlesUrl}/new`);
    await page.$eval('#name', e => e.value = 'random name');
    await page.$eval('#category', e => e.value = '2');
    await page.$eval('#content', e => e.value = 'random content');
    await page.click(".btn[value='Create']");
    await page.waitForSelector('#articles');
    expect(page.url()).toEqual(appArticlesUrl);

    const table = await page.$$('#articles tr');
    expect(table.length).toBeGreaterThan(5);

    await page.$x("//td[contains(., 'random name')]");
  });

  test('Мы можем отредактировать статью. После этого данные на странице со всеми статьями меняются', async () => {
    await page.goto(`${appArticlesUrl}/7/edit`);
    await page.type('#name', 'new name');
    await page.click(".btn[value='Update']");
    await page.waitForSelector('#articles');
    expect(page.url()).toEqual(appArticlesUrl);

    const foundAfterEdit = await page.evaluate(() => window.find('new name'));
    expect(foundAfterEdit).toBeTruthy();
  });
  // END

  afterAll(async () => {
    await browser.close();
    await app.close();
  });
});
