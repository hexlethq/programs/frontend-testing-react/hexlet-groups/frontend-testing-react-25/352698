const fs = require('fs');

// BEGIN
function upVersion(pathFile, updateType = 'patch') {
  const fileRawData = fs.readFileSync(pathFile, 'utf-8');
  const packageJson = JSON.parse(fileRawData);
  const { version } = packageJson;

  let updateVersion;
  const [major, minor, patch] = packageJson.version.split('.').map(Number);

  switch (updateType) {
    case 'major':
      updateVersion = [major + 1, 0, 0].join('.');
      break;

    case 'minor':
      updateVersion = [major, minor + 1, 0].join('.');
      break;

    case 'patch':
      updateVersion = [major, minor, patch + 1].join('.');
      break;
  };
  
  packageJson.version = [major, minor, patch].join('.');

  fs.writeFileSync(pathFile, JSON.stringify({ ...packageJson, version: updateVersion }), 'utf-8');

  return packageJson.version;
}
// END

module.exports = { upVersion };
