const fs = require('fs');
const path = require('path');
const { upVersion } = require('../src/index.js');

// BEGIN
const getFixturePath = (filename) => path.join(path.resolve(), '__fixtures__', filename);
const readFile = (filename) => fs.readFileSync(getFixturePath(filename), 'utf-8');


describe('upVersion test', () => {

  test('обновление patch версии', () => {
    upVersion(getFixturePath('package.json'));
    const updated = readFile('package.json');
    const { version } = JSON.parse(updated);
    expect(version).toEqual('1.3.3');
  });

  test('обновление minor версии', () => {
    upVersion(getFixturePath('package.json'), 'minor');
    const updated = readFile('package.json');
    const { version } = JSON.parse(updated);
    expect(version).toEqual('1.4.0');
  });

  test('обновление major версии', () => {
    upVersion(getFixturePath('package.json'), 'major');
    const updated = readFile('package.json');
    const { version } = JSON.parse(updated);
    expect(version).toEqual('2.0.0');
  });

  test('', () => {
    expect(() => getFixturePath().toThrow('Invalid format'));
  });
});

// END
